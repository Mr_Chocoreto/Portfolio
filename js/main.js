let Botones = document.getElementsByClassName('Titles');
let Contenedor = document.getElementById('Content');
let textcontent = document.getElementById('textcontent');


for(let i = 0;i<Botones.length;i++){
    Botones[i].addEventListener('click', function(){

        
        
        for (let index = 0; index < Botones[i].classList.length; index++) {
            switch (Botones[i].classList[index]) {
                case 'about':
                    getAbout();
                    break;
                    
                case 'skills':
                    getSkills();
                break;

                case 'proyects':
                    getProyects();
                break;

                case 'contact':
                    getContact();
                break;
            }
        }
        
    })
}




function getAbout(){
    
    Contenedor.classList.remove("oculto");   
    Contenedor.classList.add("oculto");
    setTimeout(function() {
    Contenedor.innerHTML = `
        
        <div class='AboutText'>
            <p>I am a game developer with 2 years of experience in the Unity game engine 
            both in 3D and 2D but mainly in the latter and I also know a little bit of other
            technologies such as Rust, C++, GDScript, Python. 
            
            </p>

            <p>I specialize in the area of library scripting and console aplications 
            that mainly build in C#.
            </p>
        </div>


        `;
    }, 300);


    
    Contenedor.classList.remove("show");
    Contenedor.classList.add("oculto");
    setTimeout(function() {
        Contenedor.classList.remove("oculto");
        Contenedor.classList.add("show");
    }, 300);
   
}


function getSkills(){

    Contenedor.classList.remove("oculto");   
    Contenedor.classList.add("oculto");
    setTimeout(function() {
    Contenedor.innerHTML = `
        
    <div class='listSkils'>
        <div class='listSkils1'>
            <h3>Programming</h3>
            <ul>
                <li>C#: Medium</li>
                <li>C++: Low</li>
                <li>Rust: Low</li>
                <li>Python: Low</li>
                <li>HTML/CSS: Low</li>
            </ul>

            <h3>Illustration</h3>
            <ul>
                <li>Digital drawing: Low</li>
                <li>Traditional drawing: Low</li>
            </ul>
        </div>

        <div class='listSkils2'>
            <h3>Content creation</h3>
            <ul>
                <li>OBS/OBS Streamlabs: High</li>
                <li>Paint Tool Sai: High</li>
                <li>Adobe Premiere: Medium</li>
                <li>After Effects: Low</li>
            </ul>

            <h3>Game engines</h3>
            <ul>
                <li>Unity 2D: Medium/High</li>
                <li>Unity 3D: Low</li>
                <li>Unreal: Low</li>
            </ul>
        </div>
    </div>

    `;
    }, 300);
    


    Contenedor.classList.remove("show");
    Contenedor.classList.add("oculto");
    setTimeout(function() {
        Contenedor.classList.remove("oculto");
        Contenedor.classList.add("show");
    }, 300);

}


function getProyects(){
    Contenedor.classList.remove("oculto");   
    Contenedor.classList.add("oculto");
    setTimeout(function() {
    Contenedor.innerHTML = `

        <div class='container'>

            <div class='slider'>

                <input type='radio' name='slider' id='sliderOne' checked>
                <input type='radio' name='slider' id='sliderTwo'>
                <input type='radio' name='slider' id='sliderThree'>

                <div class='buttons'>
                    <label for='sliderOne'></label>
                    <label for='sliderTwo'></label>
                    <label for='sliderThree'></label>
                </div>

                <div class='content'>
                </div>

                <a href='https://github.com/MrChocoreto/Compilador' target='_blank' class='con_title1'>Compilator in Unity
                <a href='https://github.com/MrChocoreto/Oss' target='_blank' class='con_title2'>0ss!
                <a href='https://github.com/MrChocoreto/Okaimono' target='_blank' class='con_title3'>Okaimono 

            </div>

        </div>
        
        `;
    }, 300);



    Contenedor.classList.remove("show");
    Contenedor.classList.add("oculto");
    setTimeout(function() {
        Contenedor.classList.remove("oculto");
        Contenedor.classList.add("show");
    }, 300);
}


function getContact(){
    Contenedor.classList.remove("oculto");   
    Contenedor.classList.add("oculto");
    setTimeout(function() {
    Contenedor.innerHTML = `
        <div class='link' style='position: relative; display: flex;'>
            <p align='center'>Discord: <a href='#'onclick='Portapapeles("dot_choco")'>dot_choco</a></p>
        </div>

        <p align='center'>Mail: <a href='https://mail.google.com/mail/u/0/?view=cm&fs=1&to=cl8122018@gmail.com' target='_blank'>cl8122018@gmail.com</a></p>
        `;
    }, 300);


    
    Contenedor.classList.remove("show");
    Contenedor.classList.add("oculto");
    setTimeout(function() {
        Contenedor.classList.remove("oculto");
        Contenedor.classList.add("show");
    }, 300);
}




function Portapapeles(texto) {
    // Crea un elemento textarea temporal
    const textarea = document.createElement('textarea');
    textarea.textContent = texto;
    document.body.appendChild(textarea);

    // Selecciona el contenido del textarea y copia al portapapeles
    textarea.select();
    document.execCommand('copy');

    // Elimina el elemento textarea
    document.body.removeChild(textarea);

    // Crea y muestra la burbuja de "Copied"
    const burbuja = document.createElement('div');
    burbuja.style.backgroundColor = '#555';
    burbuja.style.color = 'white';
    burbuja.style.textAlign = 'center';
    burbuja.style.borderRadius = '6px';
    burbuja.style.padding = '5px';
    burbuja.style.position = 'absolute';
    burbuja.style.zIndex = '1';
    burbuja.style.bottom = '85%';
    burbuja.style.left = '56%';
    burbuja.style.marginLeft = '-30px';
    burbuja.style.opacity = '0';
    burbuja.style.transition = 'opacity 0.3s';
    burbuja.textContent = 'Copied';
    const link = document.querySelector('.link');
    link.appendChild(burbuja);

    setTimeout(() => {
      burbuja.style.opacity = '1';
    }, 100);

    // Oculta la burbuja después de un tiempo y elimina el elemento
    setTimeout(() => {
      burbuja.style.opacity = '0';
      setTimeout(() => {
        link.removeChild(burbuja);
      }, 300);
    }, 1000);
}